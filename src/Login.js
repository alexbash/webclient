import React, { Component } from 'react';
import {Form,  Button, Container, Col} from 'react-bootstrap';


class Login extends Component{
    constructor(props){
        super(props);
        this.state ={
            login: "",
            password: ""
        }
    }

    handleChange = (event)=>{
        let formid = event.target.id;        
        if (formid === "login"){
            this.setState({login: event.target.value})
        }
        if (formid === "password"){
            this.setState({password: event.target.value})
        }        
    }

    handleSubmit = (event)=>{
        event.preventDefault();
        console.log("input:  " + this.state.login + " " + this.state.password)
        //insert handler request and response from server

    }

    render(){
        return(
            <div >
                <Container >
                <Col md="auto">
                <Form onSubmit={this.handleSubmit}>                    
                    <Form.Group >
                        <Form.Label>Логин Ветис.API</Form.Label>
                        <Form.Control placeholder="Enter login" id="login" value={this.state.login} onChange={this.handleChange}/>
                        {/* <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text> */}
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" id="password" placeholder="Password" value={this.state.password} onChange={this.handleChange}/>
                    </Form.Group>
                    {/* <Form.Group controlId="formBasicChecbox">
                        <Form.Check type="checkbox" label="Check me out" name=""/>
                    </Form.Group> */}
                    <Button variant="primary" type="submit">
                        Войти
                    </Button>
                </Form>
                </Col>
                </Container>
            </div>
        )
    };
    
}

export default Login;