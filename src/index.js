import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch} from "react-router-dom";
import Login from './Login';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootswatch/dist/lumen/bootstrap.min.css';
import { Navbar } from 'react-bootstrap';


ReactDOM.render((
    <BrowserRouter>
        <Route component={()=> (<Navbar bg="primary" variant="dark">
            <Navbar.Brand>Сервис реестра РСХН</Navbar.Brand>
        </Navbar>)} />
        <Switch>
            <Route  exact path='/'  component={App} />
            <Route path='/login' component={Login} />
        </Switch>              
    </BrowserRouter>
    ), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
