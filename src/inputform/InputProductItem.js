import React, { Component } from 'react';
import { Col, InputGroup, Form, Container, Button,  } from 'react-bootstrap';
import isUUID from 'validator/lib/isUUID';
import ViewData from '../viewdata/ViewData'
import Loader from '../Loader/Loader';


class InputProductItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: '',
            isValid: false,
            isInvalid: false,
            data: {},
            isLoading: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.validation = this.validation.bind(this);
        this.handlePaste = this.handlePaste.bind(this);
    };

    validation(input){
        
        if (isUUID(input)) {
            this.setState({ isValid: true, isInvalid: false});          
        }
        else {
            this.setState({isInvalid: true, isValid: false});            
        }
    };

    handlePaste (event) {
        // const clipboardData = event.clipboardData || window.clipboardData;
        // const input = clipboardData.getData("Text");
        // // console.log("Something: "+ input);        
        // // console.log(this.state.value);
    };

    handleChange (event) {           
       this.setState({ value: event.target.value}, this.validation(event.target.value));
    //    console.log("Change form: " + event.target.value);       
    };   
    
    handleClick() {
        if (!this.state.isInvalid){
        const urlrequest = this.props.endpoint + this.state.value;                  
         fetch(urlrequest)
            .then((response)=>response.json())            
            .then( result => {
                this.setState({data: result, isLoading: true})
                console.log('Request succeeded with JSON response', this.state.data);
            }).catch( (error) => {
                console.log('Request failed', error);
            });
        } else return;    
    }
    render() {
        
            
        
        return (
            <div>
            <Container  >
                <Col style={{ textAlign: 'center' }}>
                    <Form.Label ><h5>Наименование продукции по GUID</h5></Form.Label>
                    <InputGroup  >
                                                
                        <Form.Control isValid = {this.state.isValid} isInvalid = {this.state.isInvalid} 
                        onPaste = {this.handlePaste} onChange={this.handleChange} placeholder="GUID" 
                        onBlur = {()=> {this.setState({isInvalid: false, isValid: false})}}  />                         
                        <InputGroup.Append>
                            <Button variant="success" onClick={this.handleClick}>Проверить</Button>
                        </InputGroup.Append>
                        <div className="invalid-feedback">Введен неверный формат GUID</div>
                        <div className="valid-feedback">GUID корректный! Проверка возможна.</div>
                    </InputGroup>
                </Col>
                <br/>
                {/* <Loader />             */}
                {
                    this.state.isLoading ? <ViewData data={this.state.data}/> : <div></div>
                }
            </Container>                 
            </div>
        );
    }
}

export default InputProductItem;