import React from 'react';
import { ListGroup } from 'react-bootstrap';

class ViewData extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            viewdata: []
        };
        
    };
    
    // componentDidMount () {         
    //     const datab = [];
    //     const i = this.props.data;
    //     const iterate = (obj) => {
    //         Object.keys(obj).forEach(key =>{
    //             if (typeof obj[key] !== 'object' && obj[key] !==null ){
    //                 datab.push(key + " => " + obj[key]);
    //                 if (typeof obj[key] === 'object' && obj[key] !== null){
    //                     datab.push(key);
    //                     iterate(obj[key]);
    //                 }

    //             }
                

    //         })
    //     }
    //     iterate(i);            
    //     this.setState({viewdata: datab});  
        
    // }
    
    render(){
        // console.log("HEAD = " + this.state.viewhead); 
        console.log("BODY = ", this.state.viewdata);
        const head = [];
        const iterator = (obj) => { 
            for (let key in obj){
                
                if (obj[key] != null && typeof obj[key] !== 'object'){
                    let value = obj[key];
                    if (key === 'createDate' || key === 'updateDate') value = new Date (value).toDateString();
                    if (key === 'active' || key === 'last') value = "Yes";
                    head.push(<div><ListGroup.Item style = {{textAlign: 'center'}}><h5>{key}</h5><strong>{value}</strong></ListGroup.Item><br /></div>);
                }else{
                    if (typeof obj[key] === 'object' && obj[key] != null){
                        head.push(<div><ListGroup.Item variant="info" style = {{textAlign: 'center'}}><h5><strong>{key}</strong></h5></ListGroup.Item><br /></div>)
                        iterator(obj[key]);
                    }
                }
            }   
        }
        iterator(this.props.data);
     
        return(
            <div>
                <ListGroup>
                  {head} 
                </ListGroup>
            </div>
        )
    };


}

export default ViewData;